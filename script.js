let feedbackAuthors = document.querySelectorAll('.authors-list-item');
let feedbacks = document.querySelectorAll('.feedback-list-item');
let feedbackArrowLeft = document.querySelector('.arrow-1');
let feedbackArrowRight = document.querySelector('.arrow-2');

feedbackArrowLeft.addEventListener('click', function left(){
    let activeAuthor = document.querySelector('.authors-list-item-active');
    let activeId = activeAuthor.getAttribute('data-feedback-author-id');
    if (activeId >= 1){
    activeAuthor.classList.remove('authors-list-item-active');
    }
    let newActiveId = activeId - 1;
    if(newActiveId >= 1){
        let newActiveAuthor = document.querySelector('[data-feedback-author-id = "'+newActiveId+'"]');
        console.log(newActiveAuthor);
        newActiveAuthor.classList.add('authors-list-item-active');
        feedbacks.forEach(function(feedback){
            let dist = (newActiveId - 1)*1160;
            feedback.style.transform ="translateX(" + (-dist) +"px)";
            
        });
        
    }
    else{
        newActiveId = 4;
        let newActiveAuthor = document.querySelector('[data-feedback-author-id = "'+newActiveId+'"]');
        console.log(newActiveAuthor);
        newActiveAuthor.classList.add('authors-list-item-active');
        feedbacks.forEach(function(feedback){
            let dist = (newActiveId - 1)*1160;
            feedback.style.transform ="translateX(" + (-dist) +"px)";
            
        });
    }
    
});

feedbackArrowRight.addEventListener('click', function right(){
    let activeAuthor = document.querySelector('.authors-list-item-active');
    let activeId = activeAuthor.getAttribute('data-feedback-author-id');
    activeId = Number(activeId);
    if (activeId <= 4){
    activeAuthor.classList.remove('authors-list-item-active');
    }
    let newActiveId = activeId + 1;
    console.log(newActiveId);
    if(newActiveId <= 4){
        let newActiveAuthor = document.querySelector('[data-feedback-author-id = "'+newActiveId+'"]');
        console.log(newActiveAuthor);
        newActiveAuthor.classList.add('authors-list-item-active');
        feedbacks.forEach(function(feedback){
            let dist = (newActiveId - 1)*1160;
            feedback.style.transform ="translateX(" + (-dist) +"px)";
            
        });
    }
    else{
        newActiveId = 1;
        let newActiveAuthor = document.querySelector('[data-feedback-author-id = "'+newActiveId+'"]');
        console.log(newActiveAuthor);
        newActiveAuthor.classList.add('authors-list-item-active');
        feedbacks.forEach(function(feedback){
            let dist = (newActiveId - 1)*1160;
            feedback.style.transform ="translateX(" + (-dist) +"px)";
            
        });
    } 
});


feedbacks.forEach(function(defaultFeedback){
        let feedbackNumber = defaultFeedback.getAttribute('data-feedback-id');
        let defaultPos = (feedbackNumber - 1)*1160;
        defaultFeedback.style.left = defaultPos + "px"; 
    });
    

feedbackAuthors.forEach(function(author){
    
    author.addEventListener("click", function active(){
        feedbackAuthors.forEach(function(authors){
            authors.classList.remove('authors-list-item-active');
        })
        author.classList.add('authors-list-item-active');
        let authorId = author.getAttribute('data-feedback-author-id');
        feedbacks.forEach(function(feedback){
            let dist = (authorId - 1)*1160;
            feedback.style.transform ="translateX(" + (-dist) +"px)";
            
        });
    });

});

let servicesMenuItems = document.querySelectorAll('.services-list-item');
let servicesInfoItems = document.querySelectorAll('.services-info-item');

servicesMenuItems.forEach(function(menuItem){
    menuItem.addEventListener("click", function activeService(){
        
        servicesMenuItems.forEach(function(menuItems){
            menuItems.classList.remove('services-list-item-active');
        });
        menuItem.classList.add('services-list-item-active');
        let servicesMenuId = menuItem.getAttribute('data-services-menu-id');
        servicesInfoItems.forEach(function(infoItem){
            infoItem.classList.remove('services-info-item-active');
            let infoId = infoItem.getAttribute('data-services-info-id');
            if (servicesMenuId === infoId){
                infoItem.classList.add('services-info-item-active');
            }
        });
    });
});

let allWorks = document.querySelectorAll('.works-list-item');
let graphicDesignWorks = document.querySelectorAll('[data-work-category="graphicDesign"]');
let webDesignWorks = document.querySelectorAll('[data-work-category="webDesign"]');
let landingPagesWorks = document.querySelectorAll('[data-work-category="landingPages"]');
let wordpressWorks = document.querySelectorAll('[data-work-category="wordpress"]');
let allButtons = document.querySelectorAll('.work-menu-list-item');
let allWorksButton = document.querySelector('[data-works-button="all"]');
let graphicDesignButton = document.querySelector('[data-works-button="graphicDesign"]');
let webDesignButton = document.querySelector('[data-works-button="webDesign"]');
let landingPagesButton = document.querySelector('[data-works-button="landingPages"]');
let wordpressButton = document.querySelector('[data-works-button="wordpress"]');

function disactivateAllButtons(){
allButtons = document.querySelectorAll('.work-menu-list-item');
allButtons.forEach(function(eachButton){
eachButton.classList.remove('work-menu-list-item-active');
});
};

function hideAllWorks() {
allWorks = document.querySelectorAll('.works-list-item');
allWorks.forEach(function(eachWork){
eachWork.style.display = "none";
});
};

allButtons.forEach(function(button){
allWorks = document.querySelectorAll('.works-list-item');
button.addEventListener("click", function showCategory(){
disactivateAllButtons();
button.classList.add('work-menu-list-item-active');
let buttonAttr = button.getAttribute('data-works-button');
hideAllWorks();
if(buttonAttr === "all"){
allWorks.forEach(function(showWork){
    showWork.style.display = "block";
})
}
else{
allWorks.forEach(function(work){
let workAttr = work.getAttribute('data-work-category');
if(workAttr === buttonAttr){
let worksToShow = document.querySelectorAll('[data-work-category="'+workAttr+'"]');
worksToShow.forEach(function(showWorks){
    showWorks.style.display = "block";
});
}
});
}
});
});



let ourWorksUlTag = document.querySelector('.works-list');
let loadMoreButton = document.querySelector('.work-button-load-more');
loadMoreButton.addEventListener("click", function loadMore(){

loadMoreButton.style.display = "none"; 

ourWorksUlTag.insertAdjacentHTML('beforeend',`<li class="works-list-item" data-work-category="graphicDesign">
            <img src="./img/work/pictures2/graphic-design1.jpg" alt="work-item-image" class="work-item-image">
        <div class="works-hover">
        <div class="works-links-flexbox">
            <a href="#" class="work-link-1"><img src="./img/work/Combined shape 7431.png" alt="work-link-icon-1"></a>
            <a href="#" class="work-link-2"><div class="white-square"></div></a>
        </div>
            <div class="work-item-title-container"><p class="work-item-title">creative design</p></div>
            <div class="work-item-description-container"><p class="work-item-description">Web Design</p></div>
        </div>
        </li>
        <li class="works-list-item" data-work-category="graphicDesign">
            <img src="./img/work/pictures2/graphic-design10.jpg" alt="work-item-image" class="work-item-image">
        <div class="works-hover">
        <div class="works-links-flexbox">
            <a href="#" class="work-link-1"><img src="./img/work/Combined shape 7431.png" alt="work-link-icon-1"></a>
            <a href="#" class="work-link-2"><div class="white-square"></div></a>
        </div>
            <div class="work-item-title-container"><p class="work-item-title">creative design</p></div>
            <div class="work-item-description-container"><p class="work-item-description">Web Design</p></div>
        </div>
        </li>
        <li class="works-list-item" data-work-category="graphicDesign">
            <img src="./img/work/pictures2/graphic-design11.jpg" alt="work-item-image" class="work-item-image">
        <div class="works-hover">
        <div class="works-links-flexbox">
            <a href="#" class="work-link-1"><img src="./img/work/Combined shape 7431.png" alt="work-link-icon-1"></a>
            <a href="#" class="work-link-2"><div class="white-square"></div></a>
        </div>
            <div class="work-item-title-container"><p class="work-item-title">creative design</p></div>
            <div class="work-item-description-container"><p class="work-item-description">Web Design</p></div>
        </div>
        </li>
        <li class="works-list-item" data-work-category="webDesign">
            <img src="./img/work/pictures2/graphic-design12.jpg" alt="work-item-image" class="work-item-image">
        <div class="works-hover">
        <div class="works-links-flexbox">
            <a href="#" class="work-link-1"><img src="./img/work/Combined shape 7431.png" alt="work-link-icon-1"></a>
            <a href="#" class="work-link-2"><div class="white-square"></div></a>
        </div>
            <div class="work-item-title-container"><p class="work-item-title">creative design</p></div>
            <div class="work-item-description-container"><p class="work-item-description">Web Design</p></div>
        </div>
        </li>
        <li class="works-list-item" data-work-category="webDesign">
            <img src="./img/work/pictures2/graphic-design2.jpg" alt="work-item-image" class="work-item-image">
        <div class="works-hover">
        <div class="works-links-flexbox">
            <a href="#" class="work-link-1"><img src="./img/work/Combined shape 7431.png" alt="work-link-icon-1"></a>
            <a href="#" class="work-link-2"><div class="white-square"></div></a>
        </div>
            <div class="work-item-title-container"><p class="work-item-title">creative design</p></div>
            <div class="work-item-description-container"><p class="work-item-description">Web Design</p></div>
        </div>
        </li>
        <li class="works-list-item" data-work-category="webDesign">
            <img src="./img/work/pictures2/graphic-design3.jpg" alt="work-item-image" class="work-item-image">
        <div class="works-hover">
        <div class="works-links-flexbox">
            <a href="#" class="work-link-1"><img src="./img/work/Combined shape 7431.png" alt="work-link-icon-1"></a>
            <a href="#" class="work-link-2"><div class="white-square"></div></a>
        </div>
            <div class="work-item-title-container"><p class="work-item-title">creative design</p></div>
            <div class="work-item-description-container"><p class="work-item-description">Web Design</p></div>
        </div>
        </li>
        <li class="works-list-item" data-work-category="landingPages">
            <img src="./img/work/pictures2/graphic-design4.jpg" alt="work-item-image" class="work-item-image">
        <div class="works-hover">
        <div class="works-links-flexbox">
            <a href="#" class="work-link-1"><img src="./img/work/Combined shape 7431.png" alt="work-link-icon-1"></a>
            <a href="#" class="work-link-2"><div class="white-square"></div></a>
        </div>
            <div class="work-item-title-container"><p class="work-item-title">creative design</p></div>
            <div class="work-item-description-container"><p class="work-item-description">Web Design</p></div>
        </div>
        </li>
        <li class="works-list-item" data-work-category="landingPages">
            <img src="./img/work/pictures2/graphic-design5.jpg" alt="work-item-image" class="work-item-image">
        <div class="works-hover">
        <div class="works-links-flexbox">
            <a href="#" class="work-link-1"><img src="./img/work/Combined shape 7431.png" alt="work-link-icon-1"></a>
            <a href="#" class="work-link-2"><div class="white-square"></div></a>
        </div>
            <div class="work-item-title-container"><p class="work-item-title">creative design</p></div>
            <div class="work-item-description-container"><p class="work-item-description">Web Design</p></div>
        </div>
        </li>
        <li class="works-list-item" data-work-category="landingPages">
            <img src="./img/work/pictures2/graphic-design6.jpg" alt="work-item-image" class="work-item-image">
        <div class="works-hover">
        <div class="works-links-flexbox">
            <a href="#" class="work-link-1"><img src="./img/work/Combined shape 7431.png" alt="work-link-icon-1"></a>
            <a href="#" class="work-link-2"><div class="white-square"></div></a>
        </div>
            <div class="work-item-title-container"><p class="work-item-title">creative design</p></div>
            <div class="work-item-description-container"><p class="work-item-description">Web Design</p></div>
        </div>
        </li>
        <li class="works-list-item" data-work-category="landingPages">
            <img src="./img/work/pictures2/graphic-design7.jpg" alt="work-item-image" class="work-item-image">
        <div class="works-hover">
        <div class="works-links-flexbox">
            <a href="#" class="work-link-1"><img src="./img/work/Combined shape 7431.png" alt="work-link-icon-1"></a>
            <a href="#" class="work-link-2"><div class="white-square"></div></a>
        </div>
            <div class="work-item-title-container"><p class="work-item-title">creative design</p></div>
            <div class="work-item-description-container"><p class="work-item-description">Web Design</p></div>
        </div>
        </li>
        <li class="works-list-item" data-work-category="wordpress">
            <img src="./img/work/pictures2/graphic-design8.jpg" alt="work-item-image" class="work-item-image">
        <div class="works-hover">
        <div class="works-links-flexbox">
            <a href="#" class="work-link-1"><img src="./img/work/Combined shape 7431.png" alt="work-link-icon-1"></a>
            <a href="#" class="work-link-2"><div class="white-square"></div></a>
        </div>
            <div class="work-item-title-container"><p class="work-item-title">creative design</p></div>
            <div class="work-item-description-container"><p class="work-item-description">Web Design</p></div>
        </div>
        </li>
        <li class="works-list-item" data-work-category="wordpress">
            <img src="./img/work/pictures2/graphic-design9.jpg" alt="work-item-image" class="work-item-image">
        <div class="works-hover">
        <div class="works-links-flexbox">
            <a href="#" class="work-link-1"><img src="./img/work/Combined shape 7431.png" alt="work-link-icon-1"></a>
            <a href="#" class="work-link-2"><div class="white-square"></div></a>
        </div>
            <div class="work-item-title-container"><p class="work-item-title">creative design</p></div>
            <div class="work-item-description-container"><p class="work-item-description">Web Design</p></div>
        </div>
        </li>`);

        
    

    allWorks = document.querySelectorAll('.works-list-item');
    let activeButton = document.querySelector('.work-menu-list-item-active');
    let activeButtonAttr = activeButton.getAttribute('data-works-button');
    hideAllWorks();
    if (activeButtonAttr === "all"){
        allWorks.forEach(function(eachWork){
            eachWork.style.display = "block";
        });
    }
    else{
    let activeWorks = document.querySelectorAll('[data-work-category="'+activeButtonAttr+'"]');
    console.log(activeWorks);
    activeWorks.forEach(function(activeWork){
        activeWork.style.display = "block";
    });
}
});